## kernel-rt installation and boot test

This test enables the rt-repository and installs kernel-rt. It then reboots and
checks if it actually booted kernel-rt. It's meant to be a simple verification
as well as a blueprint to run other tests against kernel-rt.


