#!/bin/sh

if test -f /run/.containerenv; then
  echo "Running inside container. Skipping test."
  exit 0
fi

running_rt_kernel() {
  if uname -v | grep -q PREEMPT_RT; then
    return 0
  else
    return 1
  fi
}

if running_rt_kernel; then
  echo "Running RT kernel!"
else
  echo "Runing non-RT kernel!"
fi

if ! running_rt_kernel && test "$TMT_REBOOT_COUNT" -lt 1; then
  if rpm --quiet --query kernel-rt; then
    echo "kernel-rt is installed. Rebooting"
    tmt-reboot
  else
    echo "Error: kernel-rt not installed!"
    exit 1
  fi

elif test "$TMT_REBOOT_COUNT" -ge 1; then
  if running_rt_kernel; then
    exit 0
  else
    exit 1
  fi
fi


