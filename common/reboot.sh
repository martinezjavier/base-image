#!/bin/bash

set -uxo pipefail

if [ "${REBOOT+1}" ] && [ "$REBOOT" == "no" ]; then
    echo -e "\n Pseudo reboot script Complete"
    exit 0
fi

if [ -z "$TMT_REBOOT_COUNT" ] || [ "$TMT_REBOOT_COUNT" -eq 0 ]; then
    tmt-reboot
fi
echo -e "\n Reboot script Complete"
